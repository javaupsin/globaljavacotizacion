/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author patrick
 */
public class Cotizacion {
    private int id;
    private String folio;
    private float precio;
    private float porpi;
    private int plazos;
    private String descripcion;

    public Cotizacion(int id, String folio, float precio, float porpi, int plazos, String descripcion) {
        this.id = id;
        this.folio = folio;
        this.precio = precio;
        this.porpi = porpi;
        this.plazos = plazos;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorpi() {
        return porpi;
    }

    public void setPorpi(float porpi) {
        this.porpi = porpi;
    }

    public int getPlazos() {
        return plazos;
    }

    public void setPlazos(int plazos) {
        this.plazos = plazos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
}
